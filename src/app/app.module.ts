import {  NO_ERRORS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ActionListComponent } from './Pages/Actions/action-list.component';
import { MenuListComponent } from './Pages/Menus/menu-list.component';
import { RouterModule,Routes } from '@angular/router';
import { ModuleListComponent } from './Pages/Modules/module-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { ModuleService } from './Services/module.service';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { ActionDisplayComponent } from './Pages/Actions/action-display/action-display.component';

const appRoutes: Routes = [
  { path: 'actions', component: ActionListComponent },
  { path: 'menus', component: MenuListComponent },
  { path: 'modules', component: ModuleListComponent },
  { path: 'actiondisplay', component: ActionDisplayComponent },
  { path: '', redirectTo: '/modules', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    ActionListComponent,
    MenuListComponent,
    ModuleListComponent,
    ActionDisplayComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    NgbModule,
    FormsModule,
    HttpClientModule,
    CommonModule,
    NgSelectModule
  ],
  providers: [ModuleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
