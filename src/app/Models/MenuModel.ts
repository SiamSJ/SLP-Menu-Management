export class MenuModel {
    name: string="";
    moduleId: number=0;
    isMainParent: boolean=false;
    isActive: boolean=false;
    url: string=""; 
    orderNumber: number=0; 
    primaryParentMenuId: any | null = null; 
    secondaryParentMenuIds: number[] | null = null;
  

  }
  