export class ActionDto {
  id: number = 0
  url: string = ""
  requestMethod: string = ""
  methodName: string = ""
  actionDisplayId:number = 0
}
  