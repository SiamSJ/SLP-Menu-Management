import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvironmentUrlService } from './environment-url.service';
import { Router } from '@angular/router';
import { MenuModel } from '../Models/MenuModel';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private httpClient: HttpClient, private envUrl: EnvironmentUrlService
    , private router: Router) { }
  getAllmenus(){
    return this.httpClient.get(this.envUrl.commonurlAddress+'/Menu/getall')
  }
  getmenu(menuId: number) {
    return this.httpClient.get(this.envUrl.commonurlAddress + `/Menu/menuById?menuId=${menuId}`);
  }
  savemenu(menuObj:MenuModel){
    return this.httpClient.post(this.envUrl.commonurlAddress+'/Menu',menuObj)
  }

  editmenu(menuId:number,menuObj:MenuModel){
    return this.httpClient.put(this.envUrl.commonurlAddress+`/Menu?menuId=${menuId}`,menuObj)

  }
  deleteMenu(menuId:number){
    return this.httpClient.delete(this.envUrl.commonurlAddress+`/Menu?menuId=${menuId}`)

  }

  getMenuByModule(moduleId:number){
    return this.httpClient.get(this.envUrl.commonurlAddress+`/Menu/getMenusByModule?moduleId=${moduleId}`)

  }
}
