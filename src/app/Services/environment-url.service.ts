import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentUrlService {

  constructor() { }

  public commonurlAddress: string = 'https://localhost:5001/api';

}
