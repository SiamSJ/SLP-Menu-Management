import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { EnvironmentUrlService } from './environment-url.service';
import { Router } from '@angular/router';
import { ModuleDto } from '../Models/ModuleDto';



@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private httpClient: HttpClient, private envUrl: EnvironmentUrlService
    , private router: Router) { }
  getAllModules(){
    return this.httpClient.get(this.envUrl.commonurlAddress+'/Module/getall')
  }
  getModule(moduleId: number) {
    return this.httpClient.get(this.envUrl.commonurlAddress + `/Module/moduleById?moduleId=${moduleId}`);
  }
  saveModule(moduleObj:ModuleDto){
    return this.httpClient.post(this.envUrl.commonurlAddress+'/Module',moduleObj)
  }

  editModule(moduleId:number,moduleObj:ModuleDto){
    return this.httpClient.put(this.envUrl.commonurlAddress+`/Module?moduleId=${moduleId}`,moduleObj)

  }

  deleteModule(moduleId:number){
    return this.httpClient.delete(this.envUrl.commonurlAddress+`/Module?moduleId=${moduleId}`)

  }



}
