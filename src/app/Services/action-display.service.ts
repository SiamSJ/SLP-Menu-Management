import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvironmentUrlService } from './environment-url.service';
import { Router } from '@angular/router';
import { ActionDisplayDto } from '../Models/ActionDisplayDto';

@Injectable({
  providedIn: 'root'
})
export class ActionDisplayService {

  constructor(private httpClient: HttpClient, private envUrl: EnvironmentUrlService
    , private router: Router) { }
  getAllActionDisplays(){
    return this.httpClient.get(this.envUrl.commonurlAddress+'/ActionDisplay/getall')
  }

  saveactionDisplay(actionDisplayObj:ActionDisplayDto){
    return this.httpClient.post(this.envUrl.commonurlAddress+'/ActionDisplay',actionDisplayObj)
  }

  editactionDisplay(actionDisplayObj:ActionDisplayDto){
    return this.httpClient.put(this.envUrl.commonurlAddress+'/ActionDisplay',actionDisplayObj)
  }

  deleteactionDisplay(actionDisplayId:number){
    return this.httpClient.delete(this.envUrl.commonurlAddress+`/ActionDisplay?actionDisplayId=${actionDisplayId}`)
  }
}
