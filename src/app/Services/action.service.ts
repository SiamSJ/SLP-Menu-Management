import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvironmentUrlService } from './environment-url.service';
import { Router } from '@angular/router';
import { ActionDto } from '../Models/ActionDto';

@Injectable({
  providedIn: 'root'
})
export class ActionService {

  constructor(private httpClient: HttpClient, private envUrl: EnvironmentUrlService
    , private router: Router) { }
  getAllActions(){
    return this.httpClient.get(this.envUrl.commonurlAddress+'/Action/getall')
  }
  getAction(actionId: number) {
    return this.httpClient.get(this.envUrl.commonurlAddress + `/Action/actionById?actionId=${actionId}`);
  }
  saveaction(actionObj:ActionDto[]){
    return this.httpClient.post(this.envUrl.commonurlAddress+'/Action',actionObj)
  }

  editaction(actionId:number,actionObj:ActionDto){
    return this.httpClient.put(this.envUrl.commonurlAddress+`/Action?actionId=${actionId}`,actionObj)

  }

  deleteAction(actionId:number){
    return this.httpClient.delete(this.envUrl.commonurlAddress+`/Action?actionId=${actionId}`)

  }
}
