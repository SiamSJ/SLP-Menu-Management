import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModuleDto } from 'src/app/Models/ModuleDto';
import { ModuleService } from 'src/app/Services/module.service';


@Component({
	selector: 'app-module-list',
	templateUrl: './module-list.component.html',
	styleUrls: ['./module-list.component.css']
})
export class ModuleListComponent implements OnInit {
	page = 1;
	pageSize = 4;
	collectionSize = 0; 
	isEditMode:boolean = false;
	moduleList: any[] = [];
	moduleListDisplayed: any[] = [];

	moduleObj: ModuleDto = new ModuleDto();


	constructor(
		private modalService: NgbModal,
		private router: Router,
		private moduleService: ModuleService
	) {}

	ngOnInit(): void {
		this.getModules();
	}



	saveModule() {
		const isValid = this.validateForm()

		if(isValid){
		if(!this.isEditMode)
		{
	    console.log("saving..")
		this.moduleService.saveModule(this.moduleObj).subscribe(
			(result: any) => { alert(result.message); 
				this.modalService.dismissAll('Save click'); 
				this.getModules()
			},
			(error: any) => {
				alert('An error occurred: ' + error.message);
			});
			}
		else{
			console.log("editing...")
			console.log(this.moduleObj.id)
			this.moduleService.editModule(this.moduleObj.id,this.moduleObj).subscribe((result:any)=>{
				alert(result.message);
				this.modalService.dismissAll('Edit click'); 
				this.getModules()
			},
			(error: any) => {
				alert('An error occurred: ' + error.error.errors);
				console.log(error.error.errors)
			})
		}
	}
	



		
	}
	AddModal(content:any){
		this.isEditMode = false;
		this.moduleObj = new ModuleDto()
		this.open(content)
	}
	EditModal(module: any,content:any) {
		this.moduleObj = JSON.parse(JSON.stringify(module));;
		this.isEditMode = true;
		this.open(content)

	}
	DeleteModule(moduleId:any){
		const confirmed = confirm('Are you sure you want to delete this module?');
		if (!confirmed) {
		  return; // Abort deletion if the user cancels
		}
		this.moduleService.deleteModule(moduleId).subscribe((result:any)=>{
			alert(result.message)
			this.getModules();
		})
	}

	getModules() {
		this.moduleService.getAllModules().subscribe((data: any) => {
			this.moduleList = data.data;
			this.collectionSize = this.moduleList.length; // Update collectionSize with the total number of modules
			this.refreshModuleList();
		});
	}

	open(content: any) {
		this.modalService
			.open(content, { ariaLabelledBy: 'modal-basic-title' })
			.result.then(
				(result) => { },
				(reason) => { }
			);
	}

	refreshModuleList() {
		// Calculate the index range for the current page's items
		const startIndex = (this.page - 1) * this.pageSize;
		const endIndex = startIndex + this.pageSize;

		// Update the moduleListDisplayed with the items for the current page
		this.moduleListDisplayed = this.moduleList.slice(startIndex, endIndex);
	}

	validateForm(){
		if(this.moduleObj.name === ""){
			alert("Name is Required")
			return false;
		}
		if(this.moduleObj.iconName === ""){
			alert("Icon Name is Required")
			return false;
		}
		if(this.moduleObj.url === ""){
			alert("Url  is Required")
			return false;
		}
		if(!(this.moduleObj.orderNumber>0)){
			alert("Order Number Should be more than 0")
			return false;
		}
		return true;
	}
}

