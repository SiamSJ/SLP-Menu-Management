import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ActionDisplayDto } from 'src/app/Models/ActionDisplayDto';
import { ActionDto } from 'src/app/Models/ActionDto';
import { ActionDisplayService } from 'src/app/Services/action-display.service';
import { ActionService } from 'src/app/Services/action.service';
import { MenuService } from 'src/app/Services/menu.service';

@Component({
  selector: 'app-action-list',
  templateUrl: './action-list.component.html',
  styleUrls: ['./action-list.component.css']
})
export class ActionListComponent implements OnInit {
	page = 1;
	pageSize = 4;
	collectionSize = 0; 
	isEditMode:boolean = false;
	isActionDisplayEditMode:boolean = false;
	actionList: any[] = [];
	menuList: any[] = [];
	actionDisplayList:any[] = []
	actionListDisplayed: any[] = [];
	secondaryModalRef: NgbModalRef|undefined;


  actionObj: ActionDto = new ActionDto();
  actionDisplayObj:ActionDisplayDto = new ActionDisplayDto()


	constructor(
		private modalService: NgbModal,
		private router: Router,
		private actionService: ActionService,
		private actionDisplayService:ActionDisplayService,
    private menuService:MenuService
	) {}

  ngOnInit(): void {
		this.getActions();
    this.getMenus();
	this.getActionDisplays();
	}
	actionsArray: any[] = [{ requestMethod: '', url: '',methodName:'' }]; 

  
	addAction() {
	  this.actionsArray.push({  requestMethod: '', url: '',methodName:'' });
	}
	getActionDisplays(){
		this.actionDisplayService.getAllActionDisplays().subscribe((data: any) => {
			this.actionDisplayList = data.data;

		});
	}
  getActions() {
		this.actionService.getAllActions().subscribe((data: any) => {
			this.actionList = data.data;
      console.log(this.actionList)
			this.collectionSize = this.actionList.length; // Update collectionSize with the total number of modules
			this.refreshModuleList();
		});
	}

  getMenus(){
    this.menuService.getAllmenus().subscribe((data: any) => {
			this.menuList = data.data;
      console.log(data.data)

		});
  }

  refreshModuleList() {
		// Calculate the index range for the current page's items
		const startIndex = (this.page - 1) * this.pageSize;
		const endIndex = startIndex + this.pageSize;

		// Update the moduleListDisplayed with the items for the current page
		this.actionListDisplayed = this.actionList.slice(startIndex, endIndex);
	}
	addNewActionName(content:any){
		this.actionDisplayObj = new ActionDisplayDto();
		// Open the modal for adding a new record (menu)
		this.openActionDisplay(content);
	}

	saveActionDisplay(){
		const isValid = this.validateActionDisplay();
		if(isValid){
			this.actionDisplayService.saveactionDisplay(this.actionDisplayObj).subscribe((result:any)=>{
				alert(result.message);
				if (this.secondaryModalRef) {
					this.secondaryModalRef.close('Save click'); 
				  }			
				  this.getActionDisplays()
			})
		}
	}

	saveAction() {
		
	
		  if (!this.isEditMode) {
			console.log('saving..');
			const isValid = this.validateForm();
		if (isValid) {
		  // Set the common "Action Name" for each element in actionsArray
		  this.actionsArray.forEach((action) => {
			action.actionDisplayId = this.actionObj.actionDisplayId;
		  });
			this.actionService.saveaction(this.actionsArray).subscribe(
			  (result: any) => {
				alert(result.message);
				this.modalService.dismissAll('Save click');
				this.actionsArray = [{ name: '', menuId: 0, requestMethod: '', url: '' }]; 

				this.getActions();
			  },
			  (error: any) => {
				alert('An error occurred: ' + error.message);
			  }
			);
		  }} else {
			console.log('editing...');
			const isValid = this.validateEditform()
			if(isValid){
				this.actionService.editaction(this.actionObj.id, this.actionObj).subscribe(
					(result: any) => {
					  alert(result.message);
					  this.modalService.dismissAll('Edit click');
					  this.getActions();
					},
					(error: any) => {
					  alert('An error occurred: ' + error.message);
					}
				  );
			}
			
		  }
		
	  }



  AddModal(content:any){
    this.isEditMode = false;
    this.actionObj = new ActionDto()
	this.getActionDisplays()
    this.open(content)
  }
  EditModal(action: any,content:any) {
    this.actionObj = JSON.parse(JSON.stringify(action));
	this.actionObj.actionDisplayId = action.actionDisplayId
	
    this.isEditMode = true;
	this.getActionDisplays()

    this.open(content)

  }
  actionDisplayEdit(action:any,actiondisplaycontent:any){
	this.actionDisplayObj.id = action.actionDisplayId
	this.actionDisplayObj.menuId = action.menuId
	this.openActionDisplay(actiondisplaycontent)
  }
  DeleteAction(actionId:any){
	const confirmed = confirm('Are you sure you want to delete this action?');
	if (!confirmed) {
	  return; // Abort deletion if the user cancels
	}
	this.actionService.deleteAction(actionId).subscribe((result:any)=>{
		alert(result.message)
		this.getActions();
	})
}

  open(content: any) {
    this.modalService
      .open(content, { size: 'xl' })
      .result.then(
        (result) => { },
        (reason) => { }
      );
  }

  openActionDisplay(actiondisplaycontent: any) {
	this.secondaryModalRef = this.modalService
	  .open(actiondisplaycontent, { ariaLabelledBy: 'modal-basic-title' });
  
	this.secondaryModalRef.result.then(
	  (result) => { },
	  (reason) => { }
	);
  }
  removeAction(index: number) {
    this.actionsArray.splice(index, 1);
  }

  validateActionDisplay(){
	if (!(this.actionDisplayObj.menuId>0)) {
		alert("Menu is Required");
		return false;
	  }
	  if (this.actionDisplayObj.name === "") {
		alert("Name is Required");
		return false;
	  }
	  return true;
  }

  validateEditform(){

	  if (this.actionObj.url === "") {
		alert("Url is Required");
		return false;
	  }
	  if (this.actionObj.requestMethod === "") {
		alert("Request Method is Required");
		return false;
	  }
	  if (this.actionObj.methodName === "") {
		alert("Name is Required");
		return false;
	  }
	  if (!(this.actionObj.actionDisplayId>0)) {
		alert("Name is Required");
		return false;
	  }
	  return true;
  }
  validateForm(){

	  if (!(this.actionObj.actionDisplayId > 0)) {
		alert("Menu is Required");
		return false;
	  }
	for (const action of this.actionsArray) {

		
		if (action.url === "") {
		  alert("Url is Required");
		  return false;
		}
		if (action.requestMethod === "") {
		  alert("Please Specify Request Method");
		  return false;
		}
		if (action.methodName === "") {
			alert("Method Name is required");
			return false;
		  }
	  }
	  return true;
}
}
