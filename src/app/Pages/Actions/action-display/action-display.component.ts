import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionDisplayDto } from 'src/app/Models/ActionDisplayDto';
import { ActionDisplayService } from 'src/app/Services/action-display.service';
import { ActionService } from 'src/app/Services/action.service';
import { MenuService } from 'src/app/Services/menu.service';

@Component({
  selector: 'app-action-display',
  templateUrl: './action-display.component.html',
  styleUrls: ['./action-display.component.css']
})
export class ActionDisplayComponent implements OnInit{
	page = 1;
	pageSize = 4;
	collectionSize = 0; 
	isEditMode:boolean = false;
	actionDisplayList: any[] = [];
	menuList: any[] = [];
	actionDisplayListDisplayed: any[] = [];


  actionDisplayObj: ActionDisplayDto = new ActionDisplayDto();
  searchQuery: string = ''; // Initialize the search query



	constructor(
		private modalService: NgbModal,
		private router: Router,
		private actionService: ActionService,
		private actionDisplayService:ActionDisplayService,
    private menuService:MenuService
	) {}
  ngOnInit(){
    this.getActionDisplays()
    this.getMenus()
  }

  getActionDisplays(){
		this.actionDisplayService.getAllActionDisplays().subscribe((data: any) => {
			this.actionDisplayList = data.data;
      console.log(this.actionDisplayList)
      this.collectionSize = this.actionDisplayList.length; // Update collectionSize with the total number of modules
			this.refreshList();

		});
	}
  refreshList() {
		// Calculate the index range for the current page's items
    
		const startIndex = (this.page - 1) * this.pageSize;
		const endIndex = startIndex + this.pageSize;

		// Update the moduleListDisplayed with the items for the current page
		this.actionDisplayListDisplayed = this.actionDisplayList.slice(startIndex, endIndex);
	}



  getMenus(){
    this.menuService.getAllmenus().subscribe((data: any) => {
			this.menuList = data.data;
      console.log(data.data)

		});
  }
  AddModal(content:any){
    this.isEditMode = false;
    this.actionDisplayObj = new ActionDisplayDto()
    this.open(content)
  }

  saveActionDisplay(){
    console.log('click')
    const isValid = this.validateForm()
    if(isValid){
      if(!this.isEditMode){
        this.actionDisplayService.saveactionDisplay(this.actionDisplayObj).subscribe((result:any)=>{
          alert(result.message);
	
          this.modalService.dismissAll('Save click');
          this.getActionDisplays()
        })
      }
      else{
        if(this.isEditMode){
          this.actionDisplayService.editactionDisplay(this.actionDisplayObj).subscribe((result:any)=>{
            alert(result.message);
            this.getActionDisplays()

            this.modalService.dismissAll('Edit click');
          })
        }
      }
    }

  }

  DeleteActionDisplay(actionId:any){
    console.log(actionId)
    this.actionDisplayService.deleteactionDisplay(actionId).subscribe((result:any)=>{
      alert(result.message);
      this.getActionDisplays()
    })
  }

  EditModal(action:any,content:any){
    this.actionDisplayObj = JSON.parse(JSON.stringify(action));
	  console.log(this.actionDisplayObj)
    this.isEditMode = true;

    this.open(content)
  }
  open(content: any) {
    this.modalService
      .open(content, { size: 'xl' })
      .result.then(
        (result) => { },
        (reason) => { }
      );
  }

  validateForm(){
    if (!(this.actionDisplayObj.menuId>0)) {
      alert("Menu is Required");
      return false;
      }
      if (this.actionDisplayObj.name === "") {
      alert("Name is Required");
      return false;
      }
      return true;
    }

}
