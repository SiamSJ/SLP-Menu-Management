import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin } from 'rxjs';
import { MenuModel } from 'src/app/Models/MenuModel';
import { MenuService } from 'src/app/Services/menu.service';
import { ModuleService } from 'src/app/Services/module.service';



@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent {
	page = 1;
	pageSize = 4;
	collectionSize = 0; 
	isEditMode:boolean = false;
    menuId:number =0;
	menuList: any[] = [];
	moduleMenuList:any[] = [];
	primaryParentList:any[] = [];
	seconddaryParentList:any[] = [];
	menuListDisplayed: any[] = [];
	tempSecondaryParentMenuIds:any[]|null = []

	dropdownList :any[]= [];
	selectedItems :any[] = [];
	secondaryModalRef: NgbModalRef|undefined;




  moduleList:any[] = [];

  menuObj: MenuModel = new MenuModel();
  secondarymenuObj: MenuModel = new MenuModel();
  selectedCar: number =0;
  


  itemSelected(e:any){
	 this.tempSecondaryParentMenuIds = this.menuObj.secondaryParentMenuIds
  }


	constructor(
		private modalService: NgbModal,
		private router: Router,
    private menuService:MenuService,
    private moduleService:ModuleService
	) {}

  ngOnInit(): void {
    this.getMenus();
    this.getModules();

	}

	loadModuleMenus(){
		this.menuService.getMenuByModule(this.menuObj.moduleId).subscribe((data: any) => {
			this.moduleMenuList = data.data;
			this.primaryParentList = this.moduleMenuList
			.filter((menu) => menu.id !== this.menuId);
		});
	}

	onPrimaryParentChange() {
		const primaryParentId = this.menuObj.primaryParentMenuId;
		if (primaryParentId !== "null") {
		  this.menuObj.secondaryParentMenuIds=null
		  this.seconddaryParentList = this.primaryParentList
		  .filter((menu) => menu.id !== parseInt(primaryParentId) && menu.id !== this.menuId)
			
		} else {
		  this.seconddaryParentList = [];
		}
	  }

	  
	  
	onMainParentCheck(){
		this.menuObj.primaryParentMenuId = null
		this.menuObj.secondaryParentMenuIds = null

		this.secondarymenuObj.primaryParentMenuId = null
		this.secondarymenuObj.secondaryParentMenuIds = null
	}

  getMenus(){
    this.menuService.getAllmenus().subscribe((data: any) => {
			this.menuList = data.data;
      		this.collectionSize = this.menuList.length; // Update collectionSize with the total number of modules
			this.refreshMenuList();

		});
  }

  getModules() {
		this.moduleService.getAllModules().subscribe((data: any) => {
			this.moduleList = data.data;

		});
	}

  refreshMenuList() {
		// Calculate the index range for the current page's items
		const startIndex = (this.page - 1) * this.pageSize;
		const endIndex = startIndex + this.pageSize;

		// Update the moduleListDisplayed with the items for the current page
		this.menuListDisplayed = this.menuList.slice(startIndex, endIndex);


	}

	addNewParent(content:any){

			this.secondarymenuObj = new MenuModel();
			// Open the modal for adding a new record (menu)
			this.openSecondary(content);
		  
	}

	saveSecondaryMenu() {
		const isValid = this.validateForm(this.secondarymenuObj)

		if (isValid) {
				console.log("saving..")
				this.menuService.savemenu(this.secondarymenuObj).subscribe(
					(result: any) => {
						alert(result.message);
						if (this.secondaryModalRef) {
							this.secondaryModalRef.close('Save click'); 
						  }			
						  			this.getMenus()
						forkJoin([
							this.menuService.getMenuByModule(this.menuObj.moduleId)

						]).subscribe((result: any) => {
							this.moduleMenuList = result[0].data;
							this.primaryParentList = this.moduleMenuList
								.filter((menu) => menu.id !== this.menuId);

							this.onPrimaryParentChange()
							this.menuObj.secondaryParentMenuIds = this.tempSecondaryParentMenuIds


						})
					});
			

		}
	}

  saveMenu() {
	const isValid = this.validateForm(this.menuObj)
    
	if(isValid){
		if(!this.isEditMode)
		{
	    console.log("saving..")
		this.menuService.savemenu(this.menuObj).subscribe(
			(result: any) => { alert(result.message); 
				this.modalService.dismissAll('Save click'); 
				this.getMenus()

			});
			}
		else{
			console.log("editing...")
			this.menuService.editmenu(this.menuId,this.menuObj).subscribe((result:any)=>{
				alert(result.message);
				this.modalService.dismissAll('Edit click'); 
				this.getMenus()
			})
		}
	}
		
	}

  AddModal(content:any){
    this.isEditMode = false;
	this.moduleMenuList = []
	this.primaryParentList = []
	this.menuId = 0
	this.seconddaryParentList = []
    this.menuObj = new MenuModel()
    this.open(content)
  }
   EditModal(menu: any,content:any) {
	this.menuId = menu.id
	
	
    this.menuObj = JSON.parse(JSON.stringify(menu));
	this.moduleMenuList = [];
	this.primaryParentList = []
    
	this.menuObj.primaryParentMenuId = menu.primaryParentMenuId
	if(!menu.primaryParentMenuId){
		this.menuObj.isMainParent = true
		
	}
	console.log(menu.primaryParentId)
	forkJoin([

		this.menuService.getMenuByModule(this.menuObj.moduleId)
  
	  ]).subscribe((result: any) => {
			this.moduleMenuList = result[0].data;
			this.primaryParentList = this.moduleMenuList
			.filter((menu) => menu.id !== this.menuId);

			this.onPrimaryParentChange()
			if( menu.secondaryParentMenuIds == null){
				this.menuObj.secondaryParentMenuIds = null
	  
			}
			else{
				this.menuObj.secondaryParentMenuIds = menu.secondaryParentMenuIds.split(',').map(Number);
	  
			}
			this.tempSecondaryParentMenuIds = this.menuObj.secondaryParentMenuIds
			this.isEditMode = true;
			console.log(this.primaryParentList)
			this.open(content)
			
	  }
	  
	  )
  }
  DeleteMenu(menuId:any){
	console.log(menuId)
	const confirmed = confirm('Are you sure you want to delete this menu?');
	if (!confirmed) {
	  return; // Abort deletion if the user cancels
	}
	this.menuService.deleteMenu(menuId).subscribe((result:any)=>{
		alert(result.message)
		this.getMenus();
	})
}

  open(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => { },
        (reason) => { }
      );
  }

  openSecondary(secondarycontent: any) {
	this.secondaryModalRef = this.modalService
	  .open(secondarycontent, { ariaLabelledBy: 'modal-basic-title' });
  
	this.secondaryModalRef.result.then(
	  (result) => { },
	  (reason) => { }
	);
  }

  validateForm(menuModel:MenuModel){
	if(menuModel.name === ""){
		alert("Name is Required")
		return false;
	}
	if(!(menuModel.orderNumber>0)){
		alert("Name is Required")
		return false;
	}

	if(!(menuModel.moduleId >0)){
		alert("Module is Required")
		return false;
	}


	return true;
}
}